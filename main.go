package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/joho/godotenv"
)


func statsPage(writer http.ResponseWriter, request *http.Request) {
	websocket, err := upgrade(writer, request)
	if err != nil {
		fmt.Fprintf(writer, "%+v\n", err)
	}
	go write(websocket)
}

func setupRoutes()  {
	http.Handle("/", http.FileServer(http.Dir("./asset")))
	http.HandleFunc("/stats", statsPage)
	log.Fatal(http.ListenAndServe(":9000", nil))
}

func main()  {
	fmt.Println("Youtube subscriber Monitor")
	godotenv.Load(".env")
	setupRoutes()
}
