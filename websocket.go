package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func upgrade(writer http.ResponseWriter, request *http.Request) (*websocket.Conn, error) {
	upgrader.CheckOrigin = func(request *http.Request) bool {
		return true
	}
	websocket, err := upgrader.Upgrade(writer, request, nil)

	return websocket, err
}

func write(connection *websocket.Conn) {
	for {
		ticker := time.NewTicker(5 * time.Second)

		for channel := range ticker.C {
			items, err := getSubscribers()
			fmt.Println("Upgrade statsPage", channel, err)
			jsonString, err := json.Marshal(items)
			err = connection.WriteMessage(websocket.TextMessage, []byte(jsonString))
			fmt.Println("Upgrade statsPage", channel, err)
		}
	}
}
