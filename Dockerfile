FROM golang:1.12.1 as builder

RUN mkdir /app
ADD . /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux  go build -o main ./...
#apk update && apk add git &&  go get github.com/gorilla/mux && go get github.com/joho/godotenv
FROM alpine:latest AS production
COPY --from=builder /app .

CMD ["./main"]
