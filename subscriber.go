package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
)

type Response struct {
	Name string  `json:"kind"`
	Items []Item `json:"items"`
}
type Item struct {
	Id    string `json:"id"`
	Name  string `json:"kind"`
	Stats Stats  `json:"statistics"`
}

type Stats struct {
	Views string `json:"viewCount"`
	Subscribers string `json:"subscriberCount"`
}

func getSubscribers() (Item, error) {
	request, err := http.NewRequest("GET", "https://www.googleapis.com/youtube/v3/channels", nil)
	if err != nil {
		return Item{},err
	}
	query := request.URL.Query()
	query.Add("key", os.Getenv("YOUTUBE_KEY"))
	query.Add("id", os.Getenv("CHANNEL_ID"))
	query.Add("part", "statistics")
	request.URL.RawQuery = query.Encode()
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return Item{},err
	}
	defer response.Body.Close()

	//fmt.Println("Body", response.Body)
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Item{}, nil
	}
	var appResponse Response
	err = json.Unmarshal(body, &appResponse)

	return appResponse.Items[0], nil
}